// création du composant fiche produit
	Vue.component('fiche-produit', {
		template: `
			<div class="col-3"> 
				<p class="jumbotron">{{ nom }}<br>
					<button v-on:click="passer_commande(nom)" v-if="role == 'commande'">Commander !</button>
				</p>
			</div>
    `,
		// propriétés
		props: ['nom', 'role'],
		methods: {
			passer_commande: function(produit) {
				// $emit() => écouteur d'évènement personnalisé, permet d'émettre vers le composant parent
				this.$emit('commande-passee', produit)
			}
    }
    
	})

// instance de vue => balise racine
	var app = new Vue({
		// liaison avec l'interface
		el:"#app",
			// L'object data est l'état de l'application Vue 
			data: {
				// les variables
				user: "Bertrand",
				produits: ["Pizza", "Hamburger", "Cheeseburger", "Tacos"],
				commandes: [],
				affichage: "display-6"
		},
		// les methods => ttes les fonctionnalités de l'instance Vue.js
		methods: {
			commander: function(produit) {
				this.commandes.push(produit);
				// console.log(produit);
			}
		}
    
	})